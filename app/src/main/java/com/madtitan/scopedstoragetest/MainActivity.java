package com.madtitan.scopedstoragetest;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import com.madtitan.scopedstoragetest.databinding.ActivityMainBinding;
import com.squareup.picasso.Picasso;

import java.io.File;

public class MainActivity extends AppCompatActivity {


    ActivityMainBinding b;
    Context context;
    private static final int SELECT_FILE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = DataBindingUtil.setContentView(this,R.layout.activity_main);
        context = this;
        try {
            b.createFile.setOnClickListener((view)->{
                Utils.logFileCreate(context);
            });


            b.pickupGalleryFile.setOnClickListener((view)->{
                try {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    ((Activity) context).startActivityForResult(intent, SELECT_FILE);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            });
            b.pickupFile.setOnClickListener((view)->{
                //b.tv.setText(Utils.readFile(context));

                try {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                photoFile = Utils.createImageFile(context);

                if (photoFile!=null){
                    intent.putExtra("FLAG_GRANT_WRITE_URI_PERMISSION",1);
                    Uri photoURI = FileProvider.getUriForFile(context,
                            Utils.FILE_PROVIDER,
                            photoFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    if (intent.resolveActivity(context.getPackageManager())!=null) {
                        //context.startActivityForResult(intent, REQUEST_CAMERA_ID);
                        activityResultLauncher.launch(intent);
                    }
                }

                }catch (Exception ex){
                    ex.printStackTrace();
                }

            });





        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activityResultLauncher!=null)
            activityResultLauncher.unregister();
    }

    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            processimage();
        }
    });

    private void processimage(){
        Log.e("---PROCESSIMAGE","IS CALLED");
        try {
            String path = Utils.PATH_IS;
            if (path!=null && path.length()>0) {
                Picasso.with(this).load(new File(path)).into(b.image);
            }Log.e("Path is ","--"+path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_FILE) {

            Uri selectedImageUri = data.getData();
            String[] projection = {MediaStore.MediaColumns.DATA};
            Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();

            Utils.PATH_IS = cursor.getString(column_index);
            cursor.close();
            Bitmap bm;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(Utils.PATH_IS, options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(Utils.PATH_IS, options);
            Picasso.with(this).load(new File(Utils.PATH_IS)).into(b.image);
        }
    }
}