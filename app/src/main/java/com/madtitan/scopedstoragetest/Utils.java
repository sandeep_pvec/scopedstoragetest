package com.madtitan.scopedstoragetest;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

class Utils {

    private static final String TAG = "Utils";
    public static String getExternalStoragePath(Context context){
        ContextWrapper cw = new ContextWrapper(context);
        File file2 = cw.getExternalFilesDir(null);
        return file2.getAbsolutePath();
    }

    public static void logFileCreate(Context context){
        try {
            Process process = Runtime.getRuntime().exec("logcat -d");

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            StringBuilder log = new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                log.append("\n");
                log.append(line);
            }

            File dir = new File(getExternalStoragePath(context));
            if (!dir.exists()) {
                createLog(TAG,"Directory not present");
                dir.mkdirs();
            }else
                createLog(TAG,"Directory already present");

            File file = new File(dir, "logfile.txt");

            FileOutputStream fileout = new FileOutputStream(file);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(String.valueOf(log));
            outputWriter.close();
            createLog(TAG,"File created");
            Toast.makeText(context, "File created", Toast.LENGTH_SHORT).show();
        }catch (Exception ex){
            ex.printStackTrace();
            Toast.makeText(context, "Exception ", Toast.LENGTH_SHORT).show();
        }
    }

    public static String readFile(Context context) {
        String result = "";
        File fileEvents = new File(getExternalStoragePath(context)+"/logfile.txt");
        StringBuilder text = new StringBuilder();
        if (fileEvents.exists()) {
            createLog(TAG, "File exists");
            try {
                BufferedReader br = new BufferedReader(new FileReader(fileEvents));
                String line;
                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
                createLog(TAG, "Exception in reading");
            }
        }else {
         text.append("File does not exists");
            createLog(TAG, "File does not exists");
        }
         result = text.toString();
        return result;
    }

    public static void createLog(String tag,String msg){
        Log.e("->"+tag,"-> "+msg);
    }

    public static File createImageFile(Context context) {
        // Create an image file name
        File image = null;
        try {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "TEST" + timeStamp + "_";
        File storageDir = new File(getExternalStoragePath(context));
        if (!storageDir.exists())
            storageDir.mkdirs();
        image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        PATH_IS = image.getAbsolutePath();


        createLog("CREATE IMG","FILE TEMP CREATED AT "+imageFileName);

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return image;
    }

    public static  String PATH_IS = "";

    public static final String FILE_PROVIDER = "com.madtitan.scopedstoragetest.fileProvider";

}
